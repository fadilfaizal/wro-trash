var carddata = "";
var pincode = "";
var profile;
var auto = false;
var item = [];
var serverapi = "http://192.168.43.23/server/api.php";

$(window).ready(function(){
    step1();
});

function ecl(data){
    if(typeof data === "string")
        if(data.indexOf("Error code: " === 0))
            return false;
    return true;
}

// Insert Card
function step1(){
    var cardcheck = setInterval(function(){
        $.ajax({
            url: "../output.json",
            dataType: "json"
        }).done(function(data){
            carddata = data;
            clearInterval(cardcheck);
            $(".step1").addClass("inserted");
            setTimeout(function(){
                if($("body").attr("step")<2)
                    $("body").attr("step","2");
                step2();
            },1000);
        });
    },200);
}

// Enter Pincode
function step2(){
    if(auto){
        setTimeout(function(){
            $(".pin").val("1234");
            $(".pinsubmit").click();
        },100);
    }
    $("#pin").submit(function(event){
        event.preventDefault();
        pincode = $(".pin").val();
        if(pincode.length !== 4){
            alert("Pincode not 4 digits long.");
        } else if(!$.isNumeric(pincode)){
            alert("Invalid pincode.");
        } else {
            $.ajax({
                url: serverapi,
                dataType: "json",
                data: {
                    user: carddata,
                    pass: pincode,
                    f: "l"
                }
            }).done(function(login){
                profile = login.Profile;
                if(!ecl(login)){
                    $(".pin").css("border-color","indianred");
                    alert("Incorrect pin code.");
                } else {
                    $(".padlock.p1").css("opacity","0");
                    $(".step2").addClass("unlocked");
                    setTimeout(function(){
                        step3();
                    },1500);
                }
            });
        }
    });
}

function step3(){
   if($("body").attr("step")<3)
        $("body").attr("step","3");
    $.ajax({
        url: "restart.php"
    })
    .done(function(){
        var cardcheck = setInterval(function(){
            $.ajax({
                url: "../output.json"
            }).done(function(itemcode){
                clearInterval(cardcheck);
                item.code = itemcode;
                $(".step3").addClass("inserted");
                step4(true);
            });
        },1);
	$("#notag").click(function(){
	    clearInterval(cardcheck);
	    step4(false);
	});
    });
}

var tag;

function step4(tags){
    tag = tags;
    setTimeout(function(){
        if($("body").attr("step")<4)
            $("body").attr("step","4");
    },1000);
    if(tag){
    $.ajax({
        url: serverapi,
        dataType: "json",
        data: {
            user: carddata,
            pass: pincode,
            f: "i",
            code: item.code
        }
    })
    .done(function(data){
        data.code = item.code;
        item = data;
        $(".v-name").html(item.name);
        $(".v-rdm").html(item.rdm);
    });
    } else {
        $(".v-name").html("General Waste");
        $(".v-rdm").html("0");
	
    }
}

function step5(){
    if($("body").attr("step")<5)
        $("body").attr("step","5");
    console.log("Starting");
    var datapid;
    if(tag){
	datapid = {pid: item.pid}
    } else {
	datapid = {pid: 4}
    }
    $.ajax({
        url: "recycle.php",
        dataType: "json",
        data: datapid
    })
    .done(function(data){
        console.log("Recycling started: "+data);
	var rcheck = setInterval(function(){
            $.ajax({
                url: "recycled.json"
            })
            .done(function(data){
		if(data !== "0"){
                    clearInterval(rcheck);
                    console.log("Python finished: "+data);
                    $.ajax({
                        url: serverapi,
                        dataType: "json",
                        data: {
                            f: "b",
                            user: carddata,
                            pass: pincode,
                            code: item.code
                        }
                    })
                    .done(function(data){
                        console.log("Labelled as disposed: "+data);
                        $(".step5").addClass("processed");
                        setTimeout(function(){
			    step6();
                            if($("body").attr("step")<6)
                                $("body").attr("step","6");
                        },500);
                    });
		}
            });
	},100);
    });
}

function step6(){
    if($("body").attr("step")<6)
        $("body").attr("step","6");
    setTimeout(function(){
        reset();
    },5000);
}

function reset(){
    window.location.href = window.location.href;
}

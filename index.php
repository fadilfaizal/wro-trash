<?php file_put_contents("../output.json",""); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Recycling Machine</title>
        <link href="style.css" rel="stylesheet" />
        <script src="jquery.js"></script>
        <script src="script.js"></script>
    </head>
    <body step="1">
        <div class="step step1">
            <h1>Insert Card to Continue</h1>
            <img class="card" src="img/card.svg" />
            <div class="cardmessage">
                Insert card to buy item.
            </div>
        </div>
        <div class="step step2">
            <h1>Enter your pincode</h1>
            <img class="padlock p1" src="img/locked.svg" />
            <img class="padlock p2" src="img/unlocked.svg" />
            <form id="pin">
                <input type="password" class="pin" placeholder="••••" maxlength="4" />
                <button type="submit" class="pinsubmit">Submit</button>
            </form>
        </div>
        <div class="step step3">
            <h1>Place item in machine</h1>
            <img src="img/input.svg" class="input" />
            <button id="notag">General Waste?</button>
        </div>
        <div class="step step4">
            <h1>Item given</h1>
            <div class="v-item">
                <span class="v-name"></span>
                <img class="v-img" src="img/placeholder.png" />
                <span class="rdm">
                    Redeem for
                    <span class="v-rdm"></span>
                    <img class="v-trsh" src="img/grapes.svg" />
                </span>
            </div>
            <button id="throw" onclick="step5(tag)">Recycle</button>
        </div>
        <div class="step step5">
            <h1>Processing your request<span class="loader">.</span></h1>
        </div>
        <div class="step step6">
            <h1>Thank you for recycling! Have a nice day!</h1>
        </div>
        <button onclick="reset()" id="reset">Restart</button>
    </body>
</html>
